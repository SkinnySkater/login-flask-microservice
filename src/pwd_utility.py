import re

def is_password_strong(password):
    length_regex = re.compile(r'.{8,}')
    uppercase_regex = re.compile(r'[A-Z]')
    lowercase_regex = re.compile(r'[a-z]')
    digit_regex = re.compile(r'[0-9]')
    white_regex = re.compile(r'[ \t\n]')
    special_regex = re.compile(r'[:!#~@#></;.?&²=\[\]\{\}$£¤%§\-_`|\\/*°,]')

    return (length_regex.search(password) is not None
            and uppercase_regex.search(password) is not None
            and lowercase_regex.search(password) is not None
            and digit_regex.search(password) is not None
            and white_regex.search(password) is None
            and special_regex.search(password) is not None)

