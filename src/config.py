import os

API_VERSION = 'v0.2'

DEBUG = True

#SWAGGER
SWAGGER = {
    "title": "Swagger JWT Authentiation App",
    "uiversion": 3,
}


PORT_SERVER = 8888
FLASK_SERVER_HOST = '0.0.0.0'

# Mail settings
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'ovila.lugard@gmail.com'
MAIL_PASSWORD = 'fwdtrehasxlrdfsv'

MAIL_DEFAULT_SENDER = 'ovila.lugard@gmail.com'

KEY = 'secret'

ACTIVATION_EXPIRE_DAYS = 1
TOKEN_EXPIRE_HOURS = 1

# Twilio count Credentials
ACCOUNT_SID = 'siiiiiidxxxxxxxx'
AUTH_TOKEN = 'auth_token'
TWILIO_PHONE_NUMBER = '+442033224402'

# MySQL configurations
MYSQL_DATABASE_USER = 'root'
MYSQL_DATABASE_PASSWORD = ''
MYSQL_DATABASE_DB = 'heals'
MYSQL_DATABASE_HOST = 'localhost'


# TEST MySQL configurations
TEST_MYSQL_DATABASE_USER = 'root'
TEST_MYSQL_DATABASE_PASSWORD = ''
TEST_MYSQL_DATABASE_DB = 'heals_test'
TEST_MYSQL_DATABASE_HOST = 'localhost'

PORT_SERVER_TEST = 5555
FLASK_SERVER_HOST_TEST = '0.0.0.0'