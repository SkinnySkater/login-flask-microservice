-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 28 Novembre 2018 à 21:28
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `heals_test`
--

CREATE DATABASE heals_test;

USE heals_test;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id_user` int(20) UNSIGNED NOT NULL,
  `tokken` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `f_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `create_date_account` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pro` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id_user`, `tokken`, `email`, `f_name`, `l_name`, `nickname`, `create_date_account`, `is_active`, `state`, `is_pro`, `password`) VALUES
(2, NULL, '2thehiphacker@gmail.com', 'ThePunk', 'Li', ' The Punk', '2018-11-26 22:12:58', 0, NULL, 0, 'pbkdf2:sha256:50000$t9230n52$6bffb0c1210dbcc6cac716858f2bd216c91f6edfbe6799b92758531f508174f2'),
(12, NULL, 'thehiphacker@gmail.com', 'ThePunk', 'Li', ' The Punk2', '2018-11-26 22:38:22', 1, NULL, 0, 'pbkdf2:sha256:50000$JTjEPk5i$71ff31fd0847eadf38f9968a71930d568da5f26adb03b29b2224db3b2bb0bcb2'),
(13, NULL, 'th5ehiphacker@gmail.com', 'ThePunk', 'Li', ' The Punk222', '2018-11-26 22:46:57', 0, NULL, 0, 'pbkdf2:sha256:50000$ANwhDXPy$f393ff6c2ac79c1c70bbc821af63c97387870f8732ae40dd5d2d26a4d2b832d5'),
(14, NULL, 'th-5ehiphacker@gmail.com', 'ThePunk', 'Lawi', 'mdr', '2018-11-26 23:14:42', 0, NULL, 0, 'pbkdf2:sha256:50000$1D6ojrrA$7f6d8616a5b5446a77aef2fbfc24d526dd9e11d15bf4cbe992ad1ee79a0a503f'),
(15, NULL, 'th-5ehiphar@gmail.com', 'ThePunk', 'Lawi', 'yop', '2018-11-26 23:20:09', 0, NULL, 0, 'pbkdf2:sha256:50000$SJwHKGRz$de92ad340b5d200ac7b313ca42dc17c1eec46cdd5d68fd7c75711e113dda4cca'),
(16, NULL, 'th-5ar@gmail.com', 'ThePunk', 'Lawi', 'yopfg', '2018-11-26 23:22:15', 0, NULL, 0, 'pbkdf2:sha256:50000$hDUw2Ewv$44f4aed87aa08255064c38ec8614dc2991063f54c0170834e8513ef61fd24090'),
(17, NULL, 'th-5areee@gmail.com', 'ThePunk', 'Lawi', 'yopfgdez', '2018-11-26 23:23:34', 0, NULL, 0, 'pbkdf2:sha256:50000$8cQwAPp6$d4e1621ad55f6077264f3be5f0d30196b5f104b6fa0326534150bd7f9151ae43');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `nickname` (`nickname`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
