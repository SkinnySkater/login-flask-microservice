class Users():
    def find_email(conn, app, email):
        cur = conn.cursor()
        query_email = 'SELECT email FROM users WHERE email = %s'
        try:
            cur.execute(query_email, email)
            res = cur.fetchall()
            cur.close()
            app.logger.info(res)
            return len(res) > 0
        except:
            cur.close()
            return False

    def find_nickname(conn, app, nickname):
        cur = conn.cursor()
        query_email = 'SELECT nickname FROM users WHERE nickname = %s'
        try:
            cur.execute(query_email, nickname)
            res = cur.fetchall()
            cur.close()
            app.logger.info("--------------------------------------------------")
            app.logger.info(res)
            return len(res) > 0
        except:
            cur.close()
            return False

    def insert_new_user(conn, app, email, fname, lname, nickname, password):
        cur = conn.cursor()
        query_insert_user = 'INSERT INTO users (id_user, f_name, l_name, email, nickname, is_active, password) VALUES (DEFAULT, %s, %s, %s, %s, 0, %s)'
        try:
            cur.execute(query_insert_user, (fname, lname, email, nickname, password))
            conn.commit()
            cur.close()
            return True
        except Exception as e:
            app.logger.error(e)
            cur.close()
            return False

    def active_user(conn, email):
        cur = conn.cursor()
        query_insert_user = 'UPDATE users SET is_active = 1 WHERE users.email = %s'
        try:
            cur.execute(query_insert_user, email)
            conn.commit()
            cur.close()
            return True
        except:
            cur.close()
            return False

    def get_user_by_email(conn, app, email):
        cur = conn.cursor()
        query_user = 'SELECT * FROM users WHERE email = %s'
        try:
            cur.execute(query_user, email)
            columns = cur.description
            user_info = [{columns[index][0]: column for index, column in enumerate(value)} for value in cur.fetchall()]
            cur.close()
            app.logger.info(user_info[0])
            return user_info[0]
        except:
            cur.close()
            return None


