import unittest
from pwd_utility import is_password_strong

class TestStrongPwd(unittest.TestCase):

    def test_small_pwd(self):
        self.assertFalse(is_password_strong("5!aA"))
    def test_withe_pwd(self):
        self.assertFalse(is_password_strong("55Aqa !aA"))

    def test_good_pwd(self):
        self.assertTrue(is_password_strong("5!aA--Aze"))

    def test_no_uppercase_pwd(self):
        self.assertFalse(is_password_strong("5!a--zeuuulol"))
    def test_no_lowercase_pwd(self):
        self.assertFalse(is_password_strong("5!ALAAJY"))
    def test_no_digit_pwd(self):
        self.assertFalse(is_password_strong("!ALAAJY!!!"))
    def test_no_digit_pwd2(self):
        self.assertFalse(is_password_strong("!ALAazAJY!!"))
    def test_no_special_pwd(self):
        self.assertFalse(is_password_strong("AAOO8NBV5"))



if __name__ == '__main__':
    unittest.main()
