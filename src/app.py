import re
import datetime
import functools
from flaskext.mysql import MySQL
import jwt
from flask import Flask, request, jsonify
from flask_mail import Mail, Message
from flask_restful import Resource, Api, abort
from werkzeug.security import generate_password_hash, check_password_hash
import logging
import _thread

from flasgger import Swagger

from user import Users
from pwd_utility import is_password_strong

app = Flask(__name__)
app.config.from_object('config')
api = Api(app)

mail = Mail(app)

file_handler = logging.FileHandler('app.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

mysql = MySQL()
mysql.init_app(app)
db = mysql.connect()


def treat_json(code, msg, data):
	req_resp = {
			'code' : code,
			'message' : msg,
			"datas" : data
		}
	return req_resp

# Needed for any activities , will be copied for further microserice
def login_required(method):
    @functools.wraps(method)
    def wrapper(self):
        header = request.headers.get('Authorization')
        _, token = header.split()
        try:
            decoded = jwt.decode(token, app.config['KEY'], algorithms='HS256')
        except jwt.DecodeError:
            abort(400, message='Token is not valid.')
        except jwt.ExpiredSignatureError:
            abort(400, message='Token is expired.')
        email = decoded['email']
        if not Users.find_email(db, app, email):
            abort(400, message='User is not found.')
        user = Users.get_user_by_email(db, app, email)
        return method(self, user)
    return wrapper

class Register(Resource):
    def post(self):
        email = str(request.form.get('email'))
        password = request.form.get('password')
        fname = request.form.get('first_name')
        lname = request.form.get('last_name')
        nickname = request.form.getlist('nickname')
        user_info = Users.get_user_by_email(db, app, email)
        if user_info:
            if user_info['is_active'] == 1 or user_info['email'] == email:
                abort(400, message='email is already in use. Active it.')
        if Users.find_nickname(db, app, nickname):
            abort(400, message='The Nickname is already in use.')
        if not re.match(r'^[A-Za-z0 -9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$', email):
            abort(400, message='email is not valid.')
        if not is_password_strong(password):
            abort(400, message='password is not valide.')

        if not Users.insert_new_user(db, app, email, fname, lname, nickname, generate_password_hash(password)):
            abort(400, message='Try again, server side exception')
        exp = datetime.datetime.utcnow() + datetime.timedelta(days=app.config['ACTIVATION_EXPIRE_DAYS'])
        encoded = jwt.encode({'email': email, 'exp': exp},
                             app.config['KEY'], algorithm='HS256')
        message = 'Hello\nactivation_code={}'.format(encoded.decode('utf-8'))
        msg = Message(recipients=[email],
                      body=message,
                      subject='Activation Code')
        app.logger.info("Sending Message to the given email.")
        try:
            _thread.start_new_thread(mail.send, (msg,))
        except:
            abort(400, message='unable to send message.')
        return treat_json(200, "SUCCESS", "{'email': " + email + "}"), 200

class Activate(Resource):
    def put(self):
        activation_code = request.json['activation_code']
        try:
            decoded = jwt.decode(activation_code, app.config['KEY'], algorithms='HS256')
        except jwt.DecodeError:
            abort(400, message='Activation code is not valid.')
        except jwt.ExpiredSignatureError:
            abort(400, message='Activation code is expired.')
        email = decoded['email']
        if not Users.active_user(db, email):
            abort(500, message='Try again, server side exception')
        req_resp, req_email = {}, {}
        req_email['email'] = email
        req_resp['code'] = 201
        req_resp['message'] = "SUCCESS"
        req_resp['data'] = req_email
        res = req_resp
        return res, 201

class Login(Resource):
    def post(self):
        email = request.form.get('email')
        password = request.form.get('password')
        app.logger.info(email)
        if not Users.find_email(db, app, email):
            abort(400, message='User is not found.')
        user = Users.get_user_by_email(db, app, email)
        if not check_password_hash(user['password'], password):
            abort(400, message='Password is incorrect.')
        exp = datetime.datetime.utcnow() + datetime.timedelta(hours=app.config['TOKEN_EXPIRE_HOURS'])
        encoded = jwt.encode({'email': email, 'exp': exp},
                             app.config['KEY'], algorithm='HS256')

        req_resp, req_email = {}, {}
        req_email['email'] = email
        req_email['token'] = encoded.decode('utf-8')
        req_resp['code'] = 201
        req_resp['message'] = "SUCCESS"
        req_resp['data'] = req_email
        res = req_resp
        # add token to response headers - so SwaggerUI can use it
        res.headers.extend({'jwt-token': encoded})
        return res, 201

class Todo(Resource):
    @login_required
    def get(self, user):
        data = {'email': user['email']}
        return treat_json(200, "SUCCESS", jsonify(data)), 200

##
## Actually setup the Api resource routing here
##
api.add_resource(Register, '/{}/register'.format(app.config['API_VERSION']))
api.add_resource(Activate, '/{}/activate'.format(app.config['API_VERSION']))
api.add_resource(Login, '/{}/login'.format(app.config['API_VERSION']))
api.add_resource(Todo, '/{}/todo'.format(app.config['API_VERSION']))


if __name__ == '__main__':
    app.run(host=app.config['FLASK_SERVER_HOST'], port=app.config['PORT_SERVER'])

