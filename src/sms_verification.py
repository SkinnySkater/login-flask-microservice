from twilio.rest import Client

def sms_check(app, number):
    # Your Account Sid and Auth Token from twilio.com/console
    account_sid = app.config['ACCOUNT_SID']
    auth_token = app.config['AUTH_TOKEN']
    client = Client(account_sid, auth_token)

    message = client.messages \
                    .create(
                         body="Join Earth's mightiest heroes. Like Kevin Bacon.",
                         from_=app.config['TWILIO_PHONE_NUMBER'],
                         to=number
                     )

    print(message.sid)